import re
from django import forms
from django.core.exceptions import ValidationError

from django.contrib.auth import forms as auth_forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from accounts.emails import send_password_reset
from accounts.models import User
from accounts.choices import (
    INDUSTRY_CHOICES, COUNTRY_COICES, COUNTRY_COICES_ISO)


class GeodataForm(forms.Form):
    country = forms.ChoiceField(
        choices=COUNTRY_COICES_ISO, label='Country', required=True)
    city = forms.CharField(max_length=20, label='City')
    address = forms.CharField(max_length=255, label='Address')
    radius = forms.CharField(
        max_length=255, label='Radiuses with comma: "r1,r2,r3"',
        required=False)


class ContactUsForm(forms.Form):
    first_name = forms.CharField(
        widget=forms.TextInput, label='First Name', required=True)
    last_name = forms.CharField(
        widget=forms.TextInput, label='Last Name', required=True)
    job_title = forms.CharField(
        widget=forms.TextInput, label='Job Title', required=True)
    email = forms.EmailField(widget=forms.EmailInput,
                             label='Professional E-mail', required=True)
    phone = forms.CharField(widget=forms.TextInput,
                            label='Phone', required=True)
    company = forms.CharField(widget=forms.TextInput,
                              label='Company', required=True)
    industry = forms.ChoiceField(
        choices=INDUSTRY_CHOICES, label='What primary industry do you serve?',
        required=True)
    country = forms.ChoiceField(
        choices=COUNTRY_COICES, label='Country', required=True)
    text = forms.CharField(widget=forms.Textarea(
        attrs={"rows": 5, "cols": 20}), label='How Can We Help?',
        required=True)


class PasswordResetForm(auth_forms.PasswordResetForm):
    helper = FormHelper()
    helper.form_class = 'login-form'
    helper.layout = Layout(
        'email',
        Submit('submit', _('Reset my password'))
    )

    # pylint: disable=arguments-differ
    def save(self, *args, **kwargs):
        """
        Generates a one-use only link for resetting password and sends to the
        Copy of Django's implementation, changed to use our own email-sending.
        """
        user_model = get_user_model()
        email = self.cleaned_data["email"]
        active_users = user_model.objects.filter(
            email__iexact=email, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue

            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = default_token_generator.make_token(user)
            send_password_reset(user, uid, token)


class SetPasswordForm(auth_forms.SetPasswordForm):
    helper = FormHelper()
    helper.form_class = 'login-form'
    helper.layout = Layout(
        'new_password1',
        Submit('submit', _('Change my password'))
    )

    def __init__(self, user, *args, **kwargs):
        super(SetPasswordForm, self).__init__(user, *args, **kwargs)

        del self.fields['new_password2']


class ChangePasswordForm(forms.ModelForm):
    class Meta:
        model = User
        fields = []

    password_old = forms.CharField(widget=forms.PasswordInput(),
                                   label=_(
                                       "Enter your old password for confirmation"),
                                   required=True)
    password_new = forms.CharField(
        widget=forms.PasswordInput(), label=_("New password"), required=True)

    helper = FormHelper()
    helper.layout = Layout(
        'password_old',
        'password_new',
        Submit('submit', _('Save changes'), css_class="btn btn-primary")
    )

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()
        password_old = cleaned_data.get('password_old')
        self.password_new = cleaned_data.get('password_new')

        # If either old or new password is None, then we get an inline error and don't want to raise ValidationError
        if password_old and self.password_new and not self.instance.check_password(password_old):
            raise forms.ValidationError(
                _("The old password you've entered is not correct!"))

        return cleaned_data

    def save(self, commit=True):
        self.instance.set_password(self.password_new)

        return super(ChangePasswordForm, self).save(commit)


class SignUpForm(UserCreationForm):
    name = forms.CharField(max_length=30, required=False,
                           help_text='Optional.')
    email = forms.EmailField(
        max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('name', 'email')


class UserEditForm(forms.Form):
    avatar = forms.ImageField(label='', required=False)
    name = forms.CharField(max_length=255, required=True)
    email = forms.EmailField(required=False)


class RequestForm(forms.Form):
    city = forms.CharField(label='City', max_length=128, required=False)
    country = CountryField(blank=True).formfield()
    address = forms.CharField(label='Address', max_length=128, required=False)
    email = forms.EmailField(label='E-mail', max_length=128, required=False)
    phone_number = forms.CharField(
        help_text="example : +44 1632 960070",
        label='Phone number', max_length=20, required=False)

    # [extending form method for custom validation]
    def clean(self):
        regex = "[+]\d"

        cd = self.cleaned_data
        phone_number = cd.get('phone_number')
        address = cd.get('address')
        city = cd.get('city')
        country = cd.get('country')

        # [phohe number validation]
        if phone_number and not re.search(regex, phone_number):
            raise ValidationError('Invalid phone number')

        # [address validation]
        if address and not city or not country:
            raise ValidationError(
                'You have to complete Country and City if you completed Address')

        return cd
