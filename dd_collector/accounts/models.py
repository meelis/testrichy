# -*- coding: utf-8 -*-
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from djmoney.models.fields import MoneyField
from django.conf import settings
from django.db.models.signals import post_save, post_delete
import stripe
from rest_framework_api_key.models import AbstractAPIKey


stripe.api_key = settings.STRIPE_SECRET_KEY


class UserManager(BaseUserManager):
    # Mostly copied from django.contrib.auth.models.UserManager

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class User(PermissionsMixin, AbstractBaseUser):
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    name = models.CharField(max_length=255)
    avatar = models.ImageField(upload_to="avatars", blank=True)

    is_staff = models.BooleanField(
        _('staff status'), default=False)
    is_active = models.BooleanField(
        _('active'),  default=True)
    date_joined = models.DateTimeField(
        _('date joined'), null=True, blank=True, default=timezone.now)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name


class Customer(models.Model):
    stripe_customer_id = models.CharField(max_length=100)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    is_waiting_custom = models.BooleanField(
        verbose_name="Customer is waiting for an answer about custom plan", default=False)
    updated_at = models.DateTimeField(auto_now=True)
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    zip = models.CharField(max_length=100)
    country = models.CharField(max_length=100)

    def __str__(self):
        return self.stripe_customer_id

    class Meta:
        verbose_name = 'Customer'
        verbose_name_plural = 'Customers'


# POST_SAVE signal for creating Stripe-Customer object
def post_save_customer_create(sender, instance, created, *args, **kwargs):
    if created:
        Customer.objects.get_or_create(user=instance)

    customer, created = Customer.objects.get_or_create(user=instance)

    if customer.stripe_customer_id is None or customer.stripe_customer_id == '':
        new_customer_id = stripe.Customer.create(
            email=instance.email, name=instance.name)
        customer.stripe_customer_id = new_customer_id['id']
        customer.save()


post_save.connect(post_save_customer_create, sender=settings.AUTH_USER_MODEL)


# POST_DELETE signal for deleting Stripe-Customer object
def post_delete_customer(sender, instance, using, *args, **kwargs):
    try:
        stripe.Customer.delete(instance.stripe_customer_id)
    except stripe.error.InvalidRequestError:
        pass


post_delete.connect(post_delete_customer, sender=Customer)


class Product(models.Model):
    stripe_product_id = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField()
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=150, db_index=True)

    TYPE_CHOICES = (
        ('service', 'Service'),
        ('good', 'Good')
    )

    type = models.CharField(
        max_length=10, choices=TYPE_CHOICES, default='service')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'


# POST_SAVE signal for creating Stripe-Product object
def post_save_product_create(sender, instance, created, *args, **kwargs):
    if instance.stripe_product_id is None or instance.stripe_product_id == '':
        product_stripe = stripe.Product.create(description=instance.description,
                                               active=instance.active,
                                               name=instance.name,
                                               type=instance.type)
        instance.stripe_product_id = product_stripe['id']
        instance.save()


post_save.connect(post_save_product_create, sender=Product)


# POST_DELETE signal for deleting Stripe-Product object
def post_delete_product(sender, instance, using, *args, **kwargs):
    try:
        stripe.Product.delete(instance.stripe_product_id)
    except stripe.error.InvalidRequestError:
        pass


post_delete.connect(post_delete_product, sender=Product)


class Plan(models.Model):
    stripe_plan_id = models.CharField(max_length=100, blank=True, null=True)
    INTERVAL_CHOICES = (
        ('day', 'Day'),
        ('week', 'Week'),
        ('month', 'Month'),
        ('year', 'Year')
    )
    interval = models.CharField(max_length=5, choices=INTERVAL_CHOICES)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    nickname = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD')

    def __str__(self):
        return self.nickname

    class Meta:
        verbose_name = 'Plan'
        verbose_name_plural = 'Plans'


class PlanItems(models.Model):
    plan = models.ForeignKey(
        Plan, related_name='plan_items', on_delete=models.CASCADE)
    value = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'Plan item'
        verbose_name_plural = 'Plan items'

    def __str__(self):
        return self.plan.nickname + ' ' + self.value


# POST_SAVE signal for creating Stripe-Plan object
def post_save_plan_create(sender, instance, created, *args, **kwargs):
    if instance.stripe_plan_id is None or instance.stripe_plan_id == '':
        plan_stripe = stripe.Plan.create(currency=instance.price.currency,
                                         amount=int(instance.price.amount)*100,
                                         active=instance.active,
                                         product=instance.product.stripe_product_id,
                                         interval=instance.interval)

        instance.stripe_plan_id = plan_stripe['id']
        instance.save()


post_save.connect(post_save_plan_create, sender=Plan)


# POST_DELETE signal for deleting Stripe-Plan object
def post_delete_plan(sender, instance, using, *args, **kwargs):
    try:
        stripe.Plan.delete(instance.stripe_plan_id)
    except stripe.error.InvalidRequestError:
        pass


post_delete.connect(post_delete_plan, sender=Plan)


class Subscription(models.Model):
    stripe_subscription_id = models.CharField(
        max_length=100, blank=True, null=True)
    stripe_customer_id = models.ForeignKey(
        Customer, verbose_name='Customer', on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False)
    plan_type = models.ForeignKey(Plan, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    trial_end = models.IntegerField()

    def __str__(self):
        return self.stripe_subscription_id

    class Meta:
        verbose_name = 'Subscription'
        verbose_name_plural = 'Subscriptions'


# POST_SAVE signal for creating Stripe-Subscription object
def post_save_subscription_create(sender, instance, created, *args, **kwargs):
    if instance.stripe_subscription_id is None or instance.stripe_subscription_id == '':
        subscription_stripe = stripe.Subscription.create(items=[{"plan": instance.plan_type.stripe_plan_id}],
                                                         customer=instance.stripe_customer_id.stripe_customer_id,
                                                         trial_end=instance.trial_end)
        instance.stripe_subscription_id = subscription_stripe['id']
        instance.save()


post_save.connect(post_save_subscription_create, sender=Subscription)


# POST_DELETE signal for deleting Stripe-Subscription object
def post_delete_subscription(sender, instance, using, *args, **kwargs):
    try:
        stripe.Subscription.delete(instance.stripe_subscription_id)
    except stripe.error.InvalidRequestError:
        pass


post_delete.connect(post_delete_subscription, sender=Subscription)


class UserApiKey(AbstractAPIKey):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE, related_name='api_keys')

    class Meta:
        verbose_name = 'api key'
        verbose_name_plural = 'api keys'


class SubscribedEmails(models.Model):
    email = models.EmailField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Subscribed E-mail'
        verbose_name_plural = 'Subscribed E-mails'
