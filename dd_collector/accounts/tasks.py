from celery import shared_task
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from accounts.tokens import account_activation_token
from django.core.mail import EmailMessage   
from django.conf import settings

@shared_task
def send_email_task(to_email, request, user):
    current_site = get_current_site(request)
    mail_subject = 'Activate your account.'
    message = render_to_string('emails/confirm_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        'token':account_activation_token.make_token(user),
    })
    send_mail(mail_subject, '',  'alextestservice@gmail.com', [to_email], fail_silently=False, html_message=message,)


@shared_task
def send_contact_us_email(request):
    mail_subject = f'{request.POST.get("email")} - Contact us'
    message = f"""
        Name: {request.POST.get('name')}\n
        Email: {request.POST.get('email')}\n
        Phone: +{request.POST.get('countryCode')} {request.POST.get('phone')}\n 
        Subject: Homepage inquiry\n 
        Message: {request.POST.get('message')} 
        """
    send_mail(mail_subject, message, settings.EMAIL_SENDER, [settings.EMAIL_RECIEVER], fail_silently=False)

    
@shared_task
def send_email_custom_plan(request):
    mail_subject = f'{request.user.email} - Custom Plan'
    message = f"""
    First Name: {request.POST.get('first_name')}\n
    Last Name: {request.POST.get('last_name')}\n
    Job Title: {request.POST.get('job_title')}\n
    Professtional Email: {request.POST.get('email')}\n 
    Phone: {request.POST.get('phone')}\n 
    Company: {request.POST.get('company')}\n 
    Industry: {request.POST.get('industry')}\n 
    Country: {request.POST.get('country')}\n 
    How Can We Help?: {request.POST.get('text')} 
    """
    send_mail(mail_subject, message, settings.EMAIL_SENDER, [settings.EMAIL_RECIEVER], fail_silently=False)
  