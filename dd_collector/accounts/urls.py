from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.contrib import admin
from accounts.forms import PasswordResetForm, SetPasswordForm

from . import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^create-api-key$', views.create_api_key, name='create-api-key'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    url(r'^create-subscription$',  views.create_subscription, name='create_subscription'),
    url(r'^update-api-key/(?P<pk>.+)$', views.update_api_key, name='update_api_key'),
    url(r'^delete-api-key/(?P<pk>.+)$', views.delete_api_key, name='delete_api_key'),
    url(r'^accounts/', include('allauth.urls')),
    url(
        r'^login/$',
        auth_views.LoginView.as_view(
            template_name='accounts/login.html', redirect_authenticated_user=True),
        name='login',
    ),
    url(r'^signup', views.sign_up, name='signup'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^edit/$', views.profile, name='edit'),
    url(r'^edit/change-password/$', views.change_password, name='change_password'),
    # Password reset
    url(
        r'^password-reset/$',
        auth_views.PasswordResetView.as_view(
            template_name='accounts/password_reset.html'),
        name='password_reset'
    ),
    url(r'^password_reset/done/$',
        auth_views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'), name='password_reset_done'),
    url(
        r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(template_name='accounts/password_reset_confirm.html'),
        name='password_reset_confirm',
    ),
    url(r'^password-reset-complete/$', auth_views.PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),
        name='password_reset_complete'),
    url(r'^dashboard$', views.dashboard, name='home'),
    url('^$', views.index, name='index'),
    url(r'^cancel-subscription$', views.cancel_subscription, name='cancel_subscription'),
    url(r'^contact-us$', views.contact_us, name='contact_us'),
    url(r'^test-products$', views.test_products, name='test_products'),
    url(r'^device-info$', views.device_info, name='device_info'),
    url(r'^geodata-info$', views.geodata_info, name='geodata_info'),
    url(r'^comprehensive-info', views.comprehensive_info, name='comprehensive_info'),
    url(r'^plans-pricing/$', views.plans_pricing, name='plans_pricing'),
    url(r'^licencing/$', views.licencing, name='licencing'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^subscribing/$', views.subscribing, name='subscribing'),
    url(r'^documentation/rest_api/$', views.rest_api, name='rest_api'),
    url(r'^documentation/authentication/$', views.authentication, name='authentication'),
    url(r'^documentation/quickstart/$', views.quickstart, name='quickstart'),
    url(r'^documentation/tracking_service/$', views.tracking_service, name='tracking_service'),
    url(r'^documentation/geodata_service/$', views.geodata_service, name='geodata_service'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
