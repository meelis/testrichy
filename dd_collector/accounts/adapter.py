from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from .models import User
from allauth.account.utils import perform_login


class MySocialAccountAdapter(DefaultSocialAccountAdapter):
    def pre_social_login(self, request, sociallogin): 
        user = sociallogin.user
        if user.id:  
            return          
        try:
            user = User.objects.get(email=user.email)  # if user exists, connect the account to the existing account and login
            sociallogin.state['process'] = 'connect'                
            perform_login(request, user, 'none')
        except User.DoesNotExist:
            pass