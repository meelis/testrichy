# Generated by Django 2.0.13 on 2019-11-21 16:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20191121_1324'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='user',
        ),
        migrations.RemoveField(
            model_name='plan',
            name='product',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='plan_type',
        ),
        migrations.RemoveField(
            model_name='subscription',
            name='stripe_customer_id',
        ),
        migrations.DeleteModel(
            name='Customer',
        ),
        migrations.DeleteModel(
            name='Plan',
        ),
        migrations.DeleteModel(
            name='Product',
        ),
        migrations.DeleteModel(
            name='Subscription',
        ),
    ]
