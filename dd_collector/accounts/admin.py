from django.contrib import admin
from django.contrib.auth import forms as auth_forms
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from accounts.models import User, Customer, Subscription, Product, Plan, PlanItems, UserApiKey


@admin.register(UserApiKey)
class UserApiKeyAdmin(admin.ModelAdmin):
    list_display = ['name', 'hashed_key']


class CustomerAdmin(admin.ModelAdmin):
    readonly_fields = ('stripe_customer_id',)
    

class SubscriptionAdmin(admin.ModelAdmin):
    readonly_fields = ('stripe_subscription_id',)


class PlanAdmin(admin.ModelAdmin):
    readonly_fields = ('stripe_plan_id',)


class ProductAdmin(admin.ModelAdmin):
    readonly_fields = ('stripe_product_id',)


admin.site.register(PlanItems)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Plan, PlanAdmin)


class UserChangeForm(auth_forms.UserChangeForm):
    # Hackish variant of builtin UserChangeForm with no username
    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)

        if 'username' in self.fields:
            del self.fields['username']


class UserCreationForm(auth_forms.UserCreationForm):
    # Hackish variant of builtin UserCreationForm with email instead of username
    class Meta:
        model = User
        fields = ("email",)

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

        if 'username' in self.fields:
            del self.fields['username']


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('name', 'avatar')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {
         'fields': ('last_login', 'date_joined')},),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }),
    )
    list_display = ('id', 'email', 'name', 'is_staff')
    search_fields = ('email', 'name')
    ordering = ('email',)

    form = UserChangeForm
    add_form = UserCreationForm


admin.site.register(User, CustomUserAdmin)
