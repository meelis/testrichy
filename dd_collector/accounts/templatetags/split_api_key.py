from django import template

register = template.Library()

@register.filter
def split_key(context):
    return context[0:-3]
    