from django import template


register = template.Library()

@register.filter
def split_plan_nickname(context):
    if '_' in context:
        context = context.replace('_', ' ')
    return context