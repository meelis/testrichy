import requests
import stripe
import uuid

from django.shortcuts import render, redirect, get_object_or_404

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.sites.shortcuts import get_current_site

from django.template.loader import render_to_string
from django.http import JsonResponse

from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode

from accounts.tasks import send_email_task, send_email_custom_plan, send_contact_us_email

from .constants import HEADERS
from .models import User, Customer, Plan, Subscription, PlanItems, UserApiKey, SubscribedEmails
from scrapper.models import MobileModel, MobileItem, MobileAttr
from .tokens import account_activation_token
from .forms import (SignUpForm, UserEditForm,
                    ContactUsForm, GeodataForm, RequestForm)
import json
import datetime
from django.contrib import messages
import time


def subscribing(request):
    if request.method == 'POST':
        data = {'exists': True}
        email = request.POST.get('email')
        subscr = SubscribedEmails.objects.filter(email=email)
        if not subscr:
            SubscribedEmails.objects.create(email=email)
            data['exists'] = False
        return JsonResponse(data)
    else:
        return redirect('index')


def sign_up(request):
    if request.user.is_authenticated:
        return redirect('home')
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            send_email_task(form.cleaned_data.get('email'), request, user)
            return render(request, 'emails/email_confirmation_sent.html', )
    else:
        form = SignUpForm()
    return render(request, 'accounts/signup.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, 'emails/email_confirmation_done.html')
    else:
        return render(request, 'emails/invalid_email_confirmation.html')


@login_required
def contact_us(request):
    form = ContactUsForm()
    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            send_email_custom_plan(request)
            customer = Customer.objects.get(user=request.user)
            customer.is_waiting_custom = True
            customer.save()
            return redirect('home')
    return render(request, 'accounts/contact_us.html', {'form': form})


@login_required
def dashboard(request):
    keys = UserApiKey.objects.filter(user=request.user).order_by('created')
    customer = Customer.objects.get(user=request.user)
    subscription = Subscription.objects.filter(stripe_customer_id=customer)
    context = {'keys': keys, 'customer': customer, 'plan': ''}
    try:
        plans = Plan.objects.all().order_by('price')
    except Plan.DoesNotExist:
        pass
    else:
        context['plans'] = plans
    try:
        plan_items = PlanItems.objects.all()
    except PlanItems.DoesNotExist:
        pass
    else:
        context['plan_items'] = plan_items
    if subscription is not None and subscription:
        context['current_plan'] = subscription[0].plan_type
    else:
        try:
            context['current_plan'] = Plan.objects.get(nickname='Free')
        except Plan.DoesNotExist:
            pass
    context['user_id'] = request.user.id
    context['account_id'] = 'b86cdf1f-91a9-4d40-bc61-fdb8ed5062a0'
    if not 'sessionId' in request.session:
        request.session['sessionId'] = uuid.uuid4().hex
        request.session.modified = True
    context['session_id'] = request.session['sessionId']
    return render(request, 'accounts/dashboard.html', context)


@login_required
def cancel_subscription(request):
    try:
        customer = Customer.objects.get(user=request.user)
        subscription = Subscription.objects.get(stripe_customer_id=customer)
        subscription.delete()
    except (Customer.DoesNotExist, Customer.DoesNotExist, ):
        return redirect('home')

    return redirect('home')


@login_required
def device_info(request):
    data = dict()
    request.session['sessionId'] = uuid.uuid4().hex
    if request.method == 'POST':
        attr = ''
        launch = ''
        data['is_valid'] = True
        context = {'components': json.loads(request.POST.get('components'))}
        models = MobileItem.objects.all()
        user_agent = context['components'][0]['value']
        for m in models:
            if m.name.lower() in user_agent.lower():
                current_model = m
                attr = MobileAttr.objects.filter(item=current_model)
                obj = m.obj
                launch = obj.launch if obj.launch else ''
                break
        if launch:
            now = datetime.date.today()
            context['launch'] = (now.year - launch.year) * \
                12 + now.month - launch.month
        if attr:
            context['attr'] = attr
        data['device_info'] = render_to_string(
            'accounts/device_info.html', context, request=request)
        return JsonResponse(data)
    return render(request, 'accounts/device_info.html')


@login_required
def profile(request):
    if request.method == 'POST':
        data = dict()
        user = get_object_or_404(User, id=request.user.id)
        edit_form = UserEditForm(request.POST, request.FILES)
        if edit_form.is_valid():
            user.name = edit_form.cleaned_data['name']
            user.email = edit_form.cleaned_data['email']
            user.avatar = request.FILES['avatar'] if request.FILES else request.user.avatar
            user.save()
            data['form_is_valid'] = True
            update_session_auth_hash(request, user)
        else:
            data['form_is_valid'] = False
            context = {'edit_form': edit_form}
            data['html_form'] = render_to_string(
                'includes/edit_profile_form.html', context, request=request)
        return JsonResponse(data)
    else:
        edit_form = UserEditForm(
            initial={'name': request.user.name, 'email': request.user.email,
                     'avatar': request.user.avatar})

    password_form = PasswordChangeForm(user=request.user)
    return render(request, 'accounts/profile.html',
                  {'edit_form': edit_form,
                   'current_site': get_current_site(request),
                   'password_form': password_form})


@login_required
def change_password(request):
    data = dict()
    if request.method == 'POST':
        password_form = PasswordChangeForm(
            data=request.POST, user=request.user)
        if password_form.is_valid():
            data['form_is_valid'] = True
            password_form.save()
            update_session_auth_hash(request, password_form.user)
        else:
            data['form_is_valid'] = False
            context = {'password_form': password_form}
            data['html_form'] = render_to_string(
                'includes/password_change_form.html', context, request=request)
        return JsonResponse(data)
    return redirect('edit')


@login_required
def create_api_key(request):
    data = dict()
    api_key, key = UserApiKey.objects.create_key(
        name=request.POST.get('name'), user=request.user)
    data['form_is_valid'] = True
    data['successed_api_key'] = render_to_string(
        'includes/success-api-key.html', context={'apiKey': key},
        request=request)
    return JsonResponse(data)


@login_required
def delete_api_key(request, pk):
    api_key = UserApiKey.objects.get(pk__startswith=pk[0:-3])
    api_key.delete()
    return redirect('home')


@login_required
def update_api_key(request, pk):
    if request.method == "POST":
        api_key = UserApiKey.objects.get(pk__startswith=pk)
        api_key.name = request.POST.get('name')
        api_key.save()
    return redirect('home')


@login_required
def create_subscription(request):
    if request.method == "POST":
        customer = Customer.objects.get(user=request.user)
        customer.address = request.POST.get('address')
        customer.city = request.POST.get('city')
        customer.country = request.POST.get('country')
        customer.zip = request.POST.get('zip')
        customer.state = request.POST.get('state')
        customer.save()
        stripe.Customer.create_source(
            customer.stripe_customer_id, source=request.POST.get('stripeToken'))
        try:
            plan = Plan.objects.get(nickname=request.POST.get('plan_type'))
        except Plan.DoesNotExist:
            return redirect('home')
        trial_end = round(time.time()) + 60 * 60 * 24 * 7
        subscription = Subscription(is_active=True,
                                    plan_type=plan,
                                    stripe_customer_id=customer,
                                    trial_end=trial_end)
        subscription.save()
    return redirect('home')


@login_required
def test_products(request):
    context = dict()
    context['user_id'] = request.user.id
    context['account_id'] = 'b86cdf1f-91a9-4d40-bc61-fdb8ed5062a0'
    if not 'sessionId' in request.session:
        request.session['sessionId'] = uuid.uuid4().hex
        request.session.modified = True
    context['sessionId'] = request.session['sessionId']
    return render(request, 'accounts/test_products.html', context=context)


@login_required
def geodata_info(request):
    if request.method == 'POST':
        form = GeodataForm(request.POST)
        radius_arr = []
        if form.is_valid():
            if request.POST.get('radius') and request.POST.get('radius') != '':
                radius_arr_dirt = request.POST.get(
                    'radius').replace(' ', '').split(',')
                radius_arr = list(
                    filter(lambda x: x.isdecimal(), radius_arr_dirt))
            else:
                r1 = 0
            address = request.POST.get('address')
            address = address.replace(',',' ')
            if(len(radius_arr) == 3 and 0 < int(radius_arr[0]) <= 10000 and 0 < int(radius_arr[1]) <= 10000 and 0 < int(radius_arr[2]) <= 10000):
                api_url = 'https://api.ddrivn.com/api/address_surroundings?address={address}'.format(
                    address=f'{address}, {request.POST["country"]} {request.POST["city"]}&r1={radius_arr[0]}&r2={radius_arr[1]}&r3={radius_arr[2]}'.replace(' ', '%20'))
            else:
                api_url = 'https://api.ddrivn.com/api/address_surroundings?address={address}'.format(
                    address=f'{request.POST["address"]}, {request.POST["country"]} {request.POST["city"]}'.replace(' ', '%20'))
            resp = requests.get(api_url, headers=HEADERS)
            context = {
                'form': form,
                'response': resp.json()
            }

            return render(request, 'accounts/geodata_info.html', context)
    else:
        form = GeodataForm()
    return render(request, 'accounts/geodata_info.html', {'form': form})


@login_required
def comprehensive_info(request):
    """ View for form rendering and sending requests"""

    # [form page rendering]
    account_id = "b86cdf1f-91a9-4d40-bc61-fdb8ed5062a0"
    if not 'sessionId' in request.session:
        request.session['sessionId'] = uuid.uuid4().hex
        request.session.modified = True
    if request.method == 'GET':
        form = RequestForm()
        return render(request, 'accounts/retrive_data.html', {
            'form': form, 'session_id': request.session['sessionId'],
            "user_id": request.user.id, "account_id": account_id
        })
    else:
        form = RequestForm(request.POST)  # [getting user's data from a form]
        if form.is_valid():
            data = {k: v[0] if len(v) == 1 else v for k,
                    v in request.POST.lists()}  # [transform QueryDict to dict for put data into request]
            data.pop('csrfmiddlewaretoken')

            # [handling situation if user didn't input address]
            if not data['address']:
                data['address'] = "{country} {city}".format(
                    country=data['country'], city=data['city'])
            city = request.POST.get('city')
            country = request.POST.get('country')
            phone = request.POST.get('phone_number')
            address = request.POST.get('address')
            email = request.POST.get('email')

            api_url = 'https://api.ddrivn.com/api/generic-data?accountId={account_id}&sessionId={sessionId}'.format(
                sessionId=request.session['sessionId'], account_id=account_id
            )  # [add needed parameters to request url]
            # [sending request and getting response from API]
            print(api_url)
            resp = requests.get(api_url, headers=HEADERS)
            request.session['sessionId'] = uuid.uuid4().hex
            request.session.modified = True
            return render(
                request, 'accounts/response_data.html',
                {'data': resp.json(), 'response_code': resp.status_code}
            )
        else:
            return render(request, 'accounts/retrive_data.html',
                          {'form': form})


def rest_api(request):
    return render(request, 'documentation/rest_api.html')


def authentication(request):
    return render(request, 'documentation/authentication.html')


def quickstart(request):
    return render(request, 'documentation/quickstart.html')


def tracking_service(request):
    return render(request, 'documentation/tracking_service.html')


def geodata_service(request):
    return render(request, 'documentation/geodata_service.html')


def index(request):
    return render(request, 'index.html')


def contact(request):
    if request.method == 'POST':
        send_contact_us_email(request)
        messages.add_message(request, messages.SUCCESS,
                             'Thanks for submiting!')
    return render(request, 'contact.html')


def plans_pricing(request):
    plans = Plan.objects.all()
    context = {'plans': plans}
    return render(request, 'plans_pricing.html', context=context)


def licencing(request):
    return render(request, 'licencing.html')
