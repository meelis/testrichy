import json

from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Write the site URL to service_config.json so that WebPack could access it'

    def handle(self, *args, **options):
        data = {
            'SERVICE_URL': settings.SITE_URL,
            'RAVEN_PUBLIC_DSN': settings.RAVEN_PUBLIC_DSN,
        }

        with open(settings.SERVICE_CONFIG_PATH, "w+") as write_file:
            json.dump(data, write_file)

        self.stdout.write(
            "Finished writing the SITE_URL and RAVEN_PUBLIC_DSN to `service_config.json`")
