# For production, change this to import from settings.production
from settings.base import *

# Add proper database name, user and password here, if necessary
# DATABASES = {
#     'default': {
#         'ENGINE': 'psqlextra.backend',
#         'HOST': 'postgres',
#         'NAME': 'dd_collector',
#         'USER': 'dd_collector',
#         'PASSWORD': 'dd_collector',
#     }
# }

# For production, override SECRET_KEY


# # For development:
# # Debug toolbar
# INSTALLED_APPS.append('debug_toolbar')
# INTERNAL_IPS = ('127.0.0.1',)
# MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

# Uncomment to send emails from your local machine.
# EMAIL_HOST_PASSWORD = 'TODO (api key)'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# AWS_ACCESS_KEY_ID = 'TODO (access key)'
# AWS_SECRET_ACCESS_KEY = 'TODO (secret key)'
