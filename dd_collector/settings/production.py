from settings.staging import *


# Allowed hosts for the site
ALLOWED_HOSTS = ['richy.ddriven.eu']

# Static site url, used when we need absolute url but lack request object, e.g. in email sending.
SITE_URL = 'https://richy.ddriven.eu'

EMAIL_HOST_PASSWORD = env.str(
    'DJANGO_EMAIL_HOST_PASSWORD', default='TODO (api key)')

RAVEN_BACKEND_DSN = env.str(
    'DJANGO_RAVEN_BACKEND_DSN', default='https://TODO:TODO@sentry.thorgate.eu/TODO')
RAVEN_PUBLIC_DSN = env.str('DJANGO_RAVEN_PUBLIC_DSN',
                           default='https://TODO@sentry.thorgate.eu/TODO')
RAVEN_CONFIG['dsn'] = RAVEN_BACKEND_DSN

# Enable S3 storage
DEFAULT_FILE_STORAGE = 'dd_collector.storages.MediaStorage'
MEDIA_ROOT = env.str('DJANGO_MEDIA_ROOT', default='')
AWS_STORAGE_BUCKET_NAME = env.str(
    'DJANGO_AWS_STORAGE_BUCKET_NAME', default='dd_collector-production')
AWS_ACCESS_KEY_ID = env.str('DJANGO_AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env.str('DJANGO_AWS_SECRET_ACCESS_KEY')
