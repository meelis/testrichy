from settings.local import *


SEND_EMAILS = False

DATABASES['default']['TEST'] = {
    'NAME': 'dd_collector_test',
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CONSTANCE_CONFIG = {
    'TWILIO_ACCOUNT_SID': ('dummy',
                           'Twilio account SID (visible on the Twilio console)'),
    'TWILIO_AUTH_TOKEN': (
        'dummy', 'Twilio authentication token (visible on the Twilio console)'),
}


DEBUG_PROPAGATE_EXCEPTIONS = True

# Avoid normal non-test celery workers getting fed tasks
# from tests.
BROKER_URL = 'memory://'
