from settings.base import *


DEBUG = False

ALLOWED_HOSTS = ['test-richy.ddriven.eu']

# Static site url, used when we need absolute url but lack request object, e.g. in email sending.
SITE_URL = 'https://test-richy.ddriven.eu'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

EMAIL_HOST_PASSWORD = env.str(
    'DJANGO_EMAIL_HOST_PASSWORD', default='TODO (api key)')
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

STATIC_URL = env.str('DJANGO_STATIC_URL', default='/assets/')

# Production logging - all INFO and higher messages go to info.log file. ERROR and higher messages additionally go to
#  error.log file plus to Sentry.
LOGGING['handlers'] = {
    'sentry': {
        'level': 'WARNING',
        'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
    },
}

LOGGING['loggers'][''] = {
    'handlers': ['console', 'sentry'],
    'level': 'INFO',
    'filters': ['require_debug_false'],
}

if env.str('DJANGO_DISABLE_FILE_LOGGING', default='n') != 'y':
    # Add file handlers as addition to sentry
    LOGGING['handlers'].update({
        'info_log': {
            'level': 'INFO',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/dd_collector/info.log',
            'formatter': 'default',
        },
        'error_log': {
            'level': 'WARNING',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/dd_collector/error.log',
            'formatter': 'default',
        },
    })
    LOGGING['loggers']['']['handlers'] = ['info_log', 'error_log', 'sentry']

# Sentry error logging
INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)
RAVEN_BACKEND_DSN = env.str('DJANGO_RAVEN_BACKEND_DSN',
                            default='https://c04d3fcfd21147169d1d0e88107e7590:510b9220dab64ea284f89162ad6e798d@sentry.thorgate.eu/160')
RAVEN_PUBLIC_DSN = env.str('DJANGO_RAVEN_PUBLIC_DSN',
                           default='https://c04d3fcfd21147169d1d0e88107e7590@sentry.thorgate.eu/160')
RAVEN_CONFIG['dsn'] = RAVEN_BACKEND_DSN

# Enable S3 storage
DEFAULT_FILE_STORAGE = 'dd_collector.storages.MediaStorage'
MEDIA_ROOT = env.str('DJANGO_MEDIA_ROOT', default='')
AWS_STORAGE_BUCKET_NAME = env.str(
    'DJANGO_AWS_STORAGE_BUCKET_NAME', default='dd_collector-staging')
AWS_ACCESS_KEY_ID = env.str('DJANGO_AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env.str('DJANGO_AWS_SECRET_ACCESS_KEY')

# File path where the write_url management cmd will write the SITE_URL for front-end to access
# If this is changed, then data-driven-collector/dd_collector/app/webpack/config.base.js will also have to be changed
# along with settings/base.py
SERVICE_CONFIG_PATH = os.path.join('/app/app', "service_config.json")
