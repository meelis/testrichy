from settings.staging import *


# Allowed hosts for the site
ALLOWED_HOSTS = ['datadrivenload.thorgate.eu']

# Static site url, used when we need absolute url but lack request object, e.g. in email sending.
SITE_URL = 'https://datadrivenload.thorgate.eu'

EMAIL_HOST_PASSWORD = env.str(
    'DJANGO_EMAIL_HOST_PASSWORD', default='TODO (api key)')

RAVEN_BACKEND_DSN = env.str(
    'DJANGO_RAVEN_BACKEND_DSN', default='https://TODO:TODO@sentry.thorgate.eu/TODO')
RAVEN_PUBLIC_DSN = env.str('DJANGO_RAVEN_PUBLIC_DSN',
                           default='https://TODO@sentry.thorgate.eu/TODO')
RAVEN_CONFIG['dsn'] = RAVEN_BACKEND_DSN

# Disable s3 storage
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
MEDIA_ROOT = env.str('DJANGO_MEDIA_ROOT', default='')


DATABASES['default']['PASSWORD'] = 'dd_collector'

# Disable geofarm
GEOCODEFARM_API_KEY = None
GEOCODEFARM_FAKE_REQUESTS = True
