"""
Django settings for dd_collector project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

import os
import sys
import environ
from celery.schedules import crontab
sys.stdout.flush()

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/


# Build paths inside the project like this: os.path.join(SITE_ROOT, ...)
SITE_ROOT = os.path.dirname(os.path.dirname(__file__))

# Load env to get settings
ROOT_DIR = environ.Path(SITE_ROOT)
env = environ.Env()

READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=True)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    # By default use django.env file from project root directory
    env.read_env(str(ROOT_DIR.path('django.env')))


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DJANGO_DEBUG', default=True)

ADMINS = (
    ('Admins', 'admin@thorgate.eu'),
)
MANAGERS = ADMINS
# subject prefix for managers & admins
EMAIL_SUBJECT_PREFIX = '[dd_collector] '

SESSION_COOKIE_NAME = 'dd_collector_ssid'

INSTALLED_APPS = [
    'accounts',
    'dd_collector',
    'djmoney',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.google',
    'crispy_forms',
    'webpack_loader',
    'stripe',
    'widget_tweaks',
    'corsheaders',
    'rest_framework',
    "rest_framework_api_key",
    'rest_framework.authtoken',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'docs',
    'constance',
    'constance.backends.database',
    'django.contrib.gis',
    'scrapper'
    # django-silk may be injected here
]


MIDDLEWARE = [
    # django-silk middleware may be injected here
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(SITE_ROOT, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django_settings_export.settings_export',
            ],
        },
    },
]


# Authentication with GOOGLE
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)
SITE_ID = 1
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_USERNAME_REQUIRED = False
SOCIALACCOUNT_QUERY_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = "none"


#Stripe
STRIPE_PUBLISHABLE_KEY = 'pk_test_um7sb2bYnLoGXa7RzvWsLPD000NCFy7aIB'
STRIPE_SECRET_KEY = 'sk_test_Wfd3jmhXV4tNxYQWrDicsFZ600TVXPmHlV'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'psqlextra.backend',
        'HOST': env.str('DJANGO_DATABASE_HOST', default='postgres'),
        'PORT': env.int('DJANGO_DATABASE_PORT', default=5432),
        'NAME': env.str('DJANGO_DATABASE_NAME', default='dd_collector'),
        'USER': env.str('DJANGO_DATABASE_USER', default='dd_collector'),
        'PASSWORD': env.str('DJANGO_DATABASE_PASSWORD', default='dd_collector')
    }
}

# CORS middleware config (needed for the tracking srcipt to POST its results to backend)
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

# Redis config (used for caching and celery)
REDIS_URL = env.str('DJANGO_REDIS_URL', default='redis://redis:6379/1')


# Celery configuration
CELERY_RESULT_BACKEND = REDIS_URL
CELERY_REDIS_CONNECT_RETRY = True
CELERYD_HIJACK_ROOT_LOGGER = False
BROKER_URL = REDIS_URL
BROKER_TRANSPORT_OPTIONS = {'fanout_prefix': True}

CELERY_TIMEZONE = 'UTC'

# Caching
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": REDIS_URL,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# Constance for dynamic settings
CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'


CONSTANCE_CONFIG = {
    'TWILIO_ACCOUNT_SID': ('', 'Twilio account SID (visible on the Twilio console)'),
    'TWILIO_AUTH_TOKEN': ('', 'Twilio authentication token (visible on the Twilio console)'),
}

CONSTANCE_CONFIG_FIELDSETS = {
    'Twilio configuration': ('TWILIO_ACCOUNT_SID', 'TWILIO_AUTH_TOKEN'),
}


# Internationalization
LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', 'English'),
    ('et', 'Eesti keel'),
)
LOCALE_PATHS = (
    'locale',
)

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Media files (user uploaded/site generated)
MEDIA_ROOT = env.str('DJANGO_MEDIA_ROOT', default='/files/media')
MEDIA_URL = env.str('DJANGO_MEDIA_URL', default='/media/')
MEDIAFILES_LOCATION = env.str('DJANGO_MEDIAFILES_LOCATION', default='media')

# In staging/prod we use S3 for file storage engine
AWS_ACCESS_KEY_ID = '<unset>'
AWS_SECRET_ACCESS_KEY = '<unset>'
AWS_STORAGE_BUCKET_NAME = '<unset>'
AWS_DEFAULT_ACL = 'public-read'
AWS_IS_GZIPPED = True
AWS_S3_ENCRYPTION = True
AWS_S3_FILE_OVERWRITE = False
AWS_S3_REGION_NAME = 'eu-central-1'
AWS_S3_SIGNATURE_VERSION = 's3v4'

AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=1209600',  # 2 weeks in seconds
}

# Static files (CSS, JavaScript, images)
STATIC_ROOT = '/files/assets'
STATIC_URL = env.str('DJANGO_STATIC_URL', default='/static/')

# For django-docs
DOCS_ROOT = str(os.path.join(SITE_ROOT, 'docs', '_build'))

STATICFILES_DIRS = (
    os.path.join(SITE_ROOT, 'static'),
    os.path.join(SITE_ROOT, 'app', 'build'),
    os.path.join(SITE_ROOT, 'docs', '_build'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('DJANGO_SECRET_KEY', default='dummy key')

AUTH_USER_MODEL = 'accounts.User'


# Static site url, used when we need absolute url but lack request object, e.g. in email sending.
SITE_URL = env.str('DJANGO_SITE_URL', default='http://127.0.0.1:8000')
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=[])

# Don't allow site's content to be included in frames/iframes.
X_FRAME_OPTIONS = 'DENY'


ROOT_URLCONF = 'dd_collector.urls'

WSGI_APPLICATION = 'dd_collector.wsgi.application'


LOGIN_REDIRECT_URL = 'home'
LOGIN_URL = 'login'
LOGOUT_REDIRECT_URL = 'login'


# Crispy-forms
CRISPY_TEMPLATE_PACK = 'bootstrap3'


# Email config
DEFAULT_FROM_EMAIL = "dd_collector <info@TODO.com>"
SERVER_EMAIL = "dd_collector server <server@TODO.com>"




SENDGRID_API_KEY = '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'
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'ddriven'
EMAIL_HOST_PASSWORD = 'S3(4/t$Z#G!8nUM'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


# Show emails in the console, but don't send them.
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# SMTP  --> This is only used in staging and production
#EMAIL_HOST = env.str('DJANGO_EMAIL_HOST', default='smtp.sparkpostmail.com')
#EMAIL_PORT = env.int('DJANGO_EMAIL_PORT', default=587)
#EMAIL_HOST_USER = env.str('DJANGO_EMAIL_HOST_USER', default='SMTP_Injection')
#EMAIL_HOST_PASSWORD = env.str('DJANGO_EMAIL_HOST_PASSWORD', default='')


# Base logging config. Logs INFO and higher-level messages to console. Production-specific additions are in
#  production.py.
#  Notably we modify existing Django loggers to propagate and delegate their logging to the root handler, so that we
#  only have to configure the root handler.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s [%(levelname)s] %(name)s:%(lineno)d %(funcName)s - %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'django': {'handlers': [], 'propagate': True},
        'django.request': {'handlers': [], 'propagate': True},
        'django.security': {'handlers': [], 'propagate': True},
    }
}

TEST_RUNNER = 'django.test.runner.DiscoverRunner'
SOCIALACCOUNT_ADAPTER = 'accounts.adapter.MySocialAccountAdapter'
# Disable a few system checks. Careful with these, only silence what your really really don't need.
# TODO: check if this is right for your project.
SILENCED_SYSTEM_CHECKS = [
    # we don't use SecurityMiddleware since security is better applied in nginx config
    'security.W001',
]

EMAIL_SENDER = 'alextestservice@gmail.com'
EMAIL_RECIEVER = 'info@datadriven.ee'

# Default values for sentry
RAVEN_BACKEND_DSN = env.str(
    'DJANGO_RAVEN_BACKEND_DSN', default='https://TODO:TODO@sentry.thorgate.eu/TODO')
RAVEN_PUBLIC_DSN = env.str('DJANGO_RAVEN_PUBLIC_DSN',
                           default='https://TODO@sentry.thorgate.eu/TODO')
RAVEN_CONFIG = {'dsn': RAVEN_BACKEND_DSN}

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': '',
        'STATS_FILE': os.path.join(SITE_ROOT, 'app', 'webpack-stats.json'),
    }
}

# All these settings will be made available to javascript app
SETTINGS_EXPORT = [
    'DEBUG',
    'SITE_URL',
    'STATIC_URL',
    'RAVEN_PUBLIC_DSN',
]


REST_FRAMEWORK = {
    "TEST_REQUEST_DEFAULT_FORMAT": "json",
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),

}

# File path where the write_url management cmd will write the SITE_URL for front-end to access
# If this is changed, then data-driven-collector/dd_collector/app/webpack/config.base.js will also have to be changed
# along with settings/staging.py
SERVICE_CONFIG_PATH = os.path.join(SITE_ROOT, 'app', 'service_config.json')


GEOCODEFARM_API_KEY = 'c888ad29-5406402eb454-605b8db833af'

POSTGRES_EXTRA_DB_BACKEND_BASE = 'django.contrib.gis.db.backends.postgis'

# This is default in postgis, and it is used internally in geodjango a lot, But it looks like it is ever defined
# as some kind of setting on Django/GeoDjango side. So i'm defining it here, so it is not just a magic number any more
POSTGIS_DEFAULT_TRANSFORMATION = 4326


# Classifier
CLASSIFIER_DATA_DIRECTORY = '/classifier-data'


ENABLE_CLASSIFICATION_API = False

# Django silk
DJANGO_SILK_ENABLED = DEBUG and env.bool('DJANGO_SILK_ENABLED', default=False)
if DJANGO_SILK_ENABLED:
    INSTALLED_APPS.append('silk')
    MIDDLEWARE.insert(0, 'silk.middleware.SilkyMiddleware')
