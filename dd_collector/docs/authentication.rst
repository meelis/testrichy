Authentication
==============

Authentication when making requests to the APIs based on the ``accountId`` value assigned to
Your enterprise. This has to be passed in with every request in one of the following two ways:

``Api-key`` header
******************

Pass the ``accountId`` value (e.g. “95edc424-aaeb-424f-9437-dbdbfe9f7fe5”) in the ``Api-key`` HTTP
header.

Example request with curl:

.. parsed-literal::

    curl -X GET \
    '\ |APIUrl|\ /api/address_surroundings?address=KAUPUNKI%2C+HELSINKI' \
    -H 'Api-Key: 95edc424-aaeb-424f-9437-dbdbfe9f7fe5'

``Authorization`` header
************************

Pass the value (e.g. “Api-Token: 95edc424-aaeb-424f-9437-dbdbfe9f7fe5”) in the
``Authorization`` HTTP header. In case of a service like Postman, you have to set the
header name to ``Authorization`` and value to ``Api-Token: <uuid>``. Postman then turns this into
the correct header on its own [#f1]_.

Example request with cURL:

.. parsed-literal::

    curl -X GET \
    '\ |APIUrl|\ /api/address_surroundings?address=KAUPUNKI%2C+HELSINKI' \
    -H 'Authorization: Api-Token: 95edc424-aaeb-424f-9437-dbdbfe9f7fe5


.. rubric:: Footnotes

.. [#f1] See the `Mozilla docs <https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization>`_ for details.
