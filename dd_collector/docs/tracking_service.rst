Tracking service
================

This page lists the documentation for the tracking service.
It will cover how to set up the javascript script on the front-end of your application so that it would start
sending data back to the Tracking service, and how to retrieve the info about the tracking (tracking data and analysis of it).

Setup
*****

Include the following in the ``head`` element of the page you wish to monitor:

.. parsed-literal::

    <script
        type="text/javascript"
        src="\ |APIUrl|\ /assets/tracking.js">
    </script>

In the bottom of the page (before the closing ``body`` tag), include the following::

    <script>
        var config = {
            'accountId': '7dbbabdb-4a52-45e8-8bfe-2b1f190c66bb',
            'sessionId': 'a5cecd90-9ad7-4e1b-82a9-f7245703f185',
            'userId': 'some-user-123',
            'pageTitle': 'Application Form 1',
            'fields': {
                'LoginFormField': 'login-form-field',
            },
        };
        da('config', config);
    </script>

Both of these are needed for the tracking to work.
Replace the variables according to instructions below.


Setup variables
-------------------

.. role:: red

* ``accountId`` :red:`*` – UUID-type value that Your organization has been assigned by Data Driven
* ``sessionId`` :red:`*` – UUID-type value that uniquely identifies the user’s current session. This should be held in the website’s session.
* ``userId`` `[optional]` – ID of the user that is currently logged in for the session (string)
* ``pageTitle`` `[optional]` – Title of the page (string); if not passed in, value of the title element in head will be used (if present)
* ``fields`` `[optional]` – Dictionary of the HTML field element names that should be tracked. The **key** should be a human-readable name
  of the form field, and the **value** should be the value of the `name` attribute of the form element. If passed in, then user's activity
  will be tracked only on these elements. If this variable is not passed in, then the form fields
  (more specifically, ``input``, ``select``, and ``textarea`` HTML elements) are discovered automatically.
  **NB!** Using this option is not recommended for SPA-type webpages as the value of `fields` is not
  refreshed during rebinding.


:red:`*` - required


Session ID
++++++++++

The session ID variable (``sessionId``) needs to correspond to how you are tracking user through a process.
It needs to be reset when the end goal of the processes is achieved (some form submitted, some action performed).
Different users cannot have the same session ID - then the data will be meaningless as it does not describe the behaviour of a single user.

The rest of the uniqueness constraints depend on Your specific needs. For example:

* If the data You need should be grouped per page load, then a new ``sessionId`` needs to be set every time a page is refreshed.
* If You want the data to be aggregated per user per form submit, then a new ``sessionId`` needs to be set only when the user clicks "Submit",
  but kept the same throughout page refreshes and other activities.


.. _tracking-actions:

Tracking actions
----------------

To track when a user has done a certain action, the ‘action’ parameter can be passed on to the da
instance::

    da('action','something_happened')


First parameter – must be the string ‘action’
Second parameter – the name of the action

In order to track when a user has clicked on terms and conditions, add the following ``onClick``
method to the a tags that pop up the terms and conditions information::

    onclick="da('action','clicked_on_terms');"

The action name must be ‘clicked_on_terms’ for this to be counted in the behavioural data results
(see below).

Tracking utilities
------------------

In some cases (when fields are added to DOM after the script has been initialized) you might need to re-initialize
tracking formfields on the page. In that case you have special command 'rebind' which you can use.
Example::

    da('rebind');


In case of single page applications, you might be manipulating history in app too and in those cases you need to use
another command to make sure page loads are properly tracked.
In those cases use this before triggering history change::

    da('pageLeave');


Unless your website is single page application or you are using javascript to manipulate DOM and history
you should not need to use those additional tracking utilities and both field events and navigating the site should be tracked automatically.


.. Note to developers: I have commented out this endpoint's description as it won't be needed to be seen
.. by clients. (This endpoint was developed for one particular client who will not be using it).
.. I am leaving it in here tho so that no information is lost as to what this endpoint does.
.. Feel free to remove it once the `/api/behavior-data` endpoint is removed.
.. Due to the commenting out, `make docs` will give you an error (`malformed substitution definition`), but should still successfully build
..
.. Retrieving behavior data
.. ************************
.. .. _api-endpoint:
..
.. API endpoint
.. ------------
..
.. The aggregated tracking results are available at
..
.. .. parsed-literal::
..
..     |APIURl|/api/behavior-data?accountId={accountId}&sessionId={sessionId}
..
..
.. * ``accountId`` :red:`*` – the account ID assigned to Your organization. If you pass the account ID in another way
..   (see :doc:`authentication`), then you do not need to put it into the URL parameters.
.. * ``sessionId`` :red:`*` – the session ID (see above for details).
.. * ``city`` `[optional]` – string representing a specific city, in english; is used in the ``fraudInformationMismatch`` calculation.
.. * ``country`` `[optional]` – ISO 3166-1 alpha-2 country code string (e.g. ‘GB’ for Great Britain) representing a specific country;
..   is used in the ``fraudInformationMismatch`` calculation.
.. * ``address`` `[optional]` – an address string that is used in the ``fraudInformationMismatch`` calculation.
..   This should contain all address info (street name, house number, postal code, etc) **except city and country**,
..   which should be in their respective parameters.
..   If this is not passed in, but both city and country are, then the combination of those is used as the address.
.. * ``email`` `[optional]` – email string used in the ``emailDataEmailExists`` check.
.. * ``phoneNumber`` `[optional]` – phone number to be used for all phone number related checks. Must have the country code prefix, e.g. '+44 1632 960070'
..
..
.. :red:`*` - required
..
..
.. **Note**: If You pass in ``address``, then it is required to also pass in both ``city`` and ``country`` parameters
.. for accurate results.
..
..
.. Example query
.. ------------------
..
.. Example cURL query:
..
.. .. parsed-literal::
..
..     curl -X GET \
..     '\ |APIUrl|\ /api/behavior-data?
..     sessionId=7e4379b9-8482-47f5-b0be-b4f9833f84b4&city=London&country=LV&
..     address=20+Great+Guildford+St%2C+London+SE1+0FD%2C+UK&email=loan.applicant%40gmail.com&phoneNumber=%2B441632960070' \
..     -H 'Authorization: Api-Token: 322a7635-aec4-4307-8c54-73cb7c043124'
..
..
.. Responses
.. ---------
..
.. 200 Success
.. +++++++++++
..
.. Returned if the user has been successfully authenticated and the ``sessionId`` is valid.
..
..
.. Example response::
..
..     {
..         "deviceType": "desktop",
..         "devicePlatform": "Linux",
..         "devicePlatformVersion": null,
..         "devicePlatformBits": null,
..         "deviceBrowser": "Chrome",
..         "deviceBrowserType": "Chrome",
..         "deviceRssReader": null,
..         "deviceVersion": "71",
..         "deviceISP": "Telecom",
..         "behaviorTermsVisited": true,
..         "behaviorPageViewTime": 78497,
..         "behaviorPagesVisited": 46,
..         "behaviorUniquePagesVisited": 1,
..         "behaviorTypingSpeed": 308,
..         "behaviorCopyPaste": 1,
..         "behaviorFieldChanges": 3,
..         "behaviorDwellTimeAvg": 159.56410256410257,
..         "behaviorFlightTimeAvg": 158.41025641025641,
..         "behaviorPauses": 0,
..         "behaviorDigraphAvg": 90.1025641025641,
..         "behaviorTrigraphAvg": 153.25641025641025,
..         "behaviorBackspaces": 11,
..         "behaviorDeletes": 11,
..         "fraudUnusualDevice": true,
..         "fraudInformationMismatch": false,
..         "fraudUnusualBehavior": true,
..         "emailDataEmailExists": true,
..         "phoneNumberCarrierName": "Verizon",
..         "phoneNumberLineType": "mobile",
..         "phoneNumberCallerName": "Delicious Cheese Cake",
..         "phoneNumberCallerType": "BUSINESS",
..         "uuid": "cc391fe5-9b59-4c94-a595-811e553caaa6",
..     }
..
..
.. Response fields:
..
.. ================================= =============================  ========================================================================================================
.. Field name                        Example value                  Description
.. ================================= =============================  ========================================================================================================
.. deviceType                        ``desktop``                    Type of the Device (desktop/mobile/tablet/touch_device)
.. devicePlatform                    ``iOS``                        Name of the Platform
.. devicePlatformVersion             ``4``                          Major version of the Platform
.. devicePlatformBits                ``x64``                        Platform Bits
.. deviceBrowser                     ``Chrome``                     Name of the Browser
.. deviceRssReader [#f1]_            ``null``                       Is the Browser RSS reader (true/false)
.. deviceVersion                     ``39``                         Major version of the browser
.. deviceISP                         ``Telecom``                    Name of the ISP associated with the IP (is ``null`` if data is asked before the IP info has been saved)
.. behaviorTermsVisited              ``false``                      Did applicant visit terms & conditions (true/false)
.. behaviorPageViewTime              ``132``                        Total time applicant spent on the site in seconds
.. behaviorPagesVisited              ``3``                          Total number of applicant’s page views
.. behaviorUniquePagesVisited        ``2``                          Total number of unique pages applicant visited
.. behaviorTypingSpeed               ``308``                        Applicant’s typing speed (key strokes per minute)
.. behaviorCopyPaste                 ``0``                          Number of fields where paste was used
.. behaviorFieldChanges              ``6``                          Number of fields edited after initial input
.. behaviorDwellTimeAvg              ``225.534``                    Average dwell time in milliseconds
.. behaviorFlightTimeAvg             ``321.11``                     Average flight time in milliseconds
.. behaviorPauses                    ``4``                          Number of pauses during writing
.. behaviorDigraphAvg                ``133``                        Average digraph time in milliseconds
.. behaviorTrigraphAvg               ``209``                        Average tri-graph time in milliseconds
.. behaviorBackspaces                ``0``                          Number of times the backspace key was pressed
.. behaviorDeletes                   ``5``                          Number of times the delete key was pressed
.. fraudInformationMismatch [#f2]_   ``false``                      Information mismatch between data sources (true/false)
.. fraudUnusualBehavior              ``false``                      Unusual applicant behavior (true/false)
.. fraudUnusualDevice                ``true``                       Unusual device information (true/false)
.. emailDataEmailExists              ``true``                       Mail server confirms email is valid (true/false/null (if no email was passed in))
.. phoneNumberCarrierName            ``Verizon``                    The name of the carrier
.. phoneNumberLineType               ``landline``                   The phone number type (landline/mobile/voip)
.. phoneNumberCallerName             ``John McLastName``            String indicating the name of the owner of the phone number
.. phoneNumberCallerType             ``CONSUMER``                   String indicating whether this caller is a business or consumer (business/consumer); only works for US phone numbers.
.. request_id                        ``cc391fe5-9b59-4c94-...``     Unique UUID of each request
.. ================================= =============================  ========================================================================================================
..
..
.. 403 Forbidden
.. +++++++++++++
..
.. Returned if user has not authenticated themselves via the request headers or the query parameters
.. (see :doc:`authentication` and :ref:`api-endpoint` for details).
.. Data will have the following message: ``Authentication credentials were not provided.``


Retrieving data
***************

API endpoint
------------

Some different aggregated tracking results are available at

.. parsed-literal::

    |APIURl|/api/generic-data?accountId={accountId}&sessionId={sessionId}


* ``accountId`` :red:`*` – the account ID assigned to Your organization. If you pass the account ID in another way
  (see :doc:`authentication`), then you do not need to put it into the URL parameters.
* ``sessionId`` :red:`*` – the session ID (see above for details).
* ``city`` `[optional]` – string representing a specific city, in english; is used in the ``location`` calculations.
* ``country`` `[optional]` – ISO 3166-1 alpha-2 country code string (e.g. ‘GB’ for Great Britain) representing a specific country;
  is used in the ``location`` calculations.
* ``address`` `[optional]` – an address string that is used in the ``location`` calculations.
  This should contain all address info (street name, house number, postal code) **except city and country**,
  which should be in their respective parameters.
  If this is not passed in, but both city and country are, then the combination of those is used as the address.
* ``email`` `[optional]` – email string used in the ``email`` checks.
* ``phoneNumber`` `[optional]` – phone number to be used for all phone number related checks. Must have the country code prefix, e.g. '+44 1632 960070'


:red:`*` - required


**Note**: If You pass in ``address``, then it is required to also pass in both ``city`` and ``country`` parameters
for accurate results.


Example query
------------------

Example cURL query:

.. parsed-literal::

    curl -X GET \
    '\ |APIUrl|\ /api/generic-data?
    sessionId=7e4379b9-8482-47f5-b0be-b4f9833f84b4&city=London&country=LV&
    address=20+Great+Guildford+St%2C+London+SE1+0FD%2C+UK&email=loan.applicant%40gmail.com&phoneNumber=%2B441632960070' \
    -H 'Authorization: Api-Token: 322a7635-aec4-4307-8c54-73cb7c043124'


Responses
---------

200 Success
+++++++++++

Returned if the user has been successfully authenticated and the ``sessionId`` is valid.


Example response::

    {
        "request_id": "f98b63b3-2dac-49c4-b47d-386fe7cd9cee",
        "_meta": null,
        "deviceSeenHomeCount": 2,
        "deviceSeenHomeData": [['2019-09-13T08:34:29.187304Z', '1.1.1.1', 30]],
        "deviceSeenAwayCount": null,
        "deviceSeenAwayData": null,
        "deviceType": "desktop",
        "devicePrice": null,
        "deviceEmailAssociation": ['some@email.com'],
        "devicePhoneAssociation": ['+55524236225'],
        "locationGeocodingAccuracy": "EXACT_MATCH",
        "locationAverageIncome": null,
        "locationRentalPrice": null,
        "locationRealEstatePrice": null,
        "locationClassification": null,
        "locationOtherCustomers": 1,
        "fraudTooFastSession": "true",
        "fraudIpChange": "false",
        "fraudBrowserTimeZone": "true",
        "fraudVPN": null,
        "fraudUncommonLanguage": "true",
        "fraudDistance": 4.5,
        "fraudCountryMismatch": "true",
        "behaviorTermsVisited": "true",
        "behaviorSessionDuration": 90,
        "behaviorTypingSpeed": 45,
        "behaviorCopyPaste": 2,
        "behaviorFieldChanges": 4,
        "emailDataCompromised": null,
        "emailDataEmailExists": "true",
        "phoneNumberIsActive": "true",
        "phoneNumberCarrierName": "Telekom AS",
        "phoneNumberLineType": "landline",
        "phoneNumberCallerName": "Maggie Smith",
        "phoneNumberCallerType": "BUSINESS",
    }


Response fields:

================================= ======================================================= ========================================================================================================
Field name                        Example value                                           Description
================================= ======================================================= ========================================================================================================
request_id                        ``f98b63b3-2dac-49c4-b47d-386fe7cd9cee``                Unique UUID of each request
_meta                             ``{}``                                                  All metadata the service has collected about this session; see :ref:`metadata` for details
deviceSeenHomeCount               ``2``                                                   Number of times device has been seen on client website
deviceSeenHomeData                ``[['2019-09-13T08:34:29.187304Z', '1.1.1.1', 30]]``    List of data about the last 100 visits to the client's page: (<start of visit in UTC>, <IP address used during visit>, <duration of visit in seconds>). If any info is missing, nothing is returned
deviceSeenAwayCount               ``1``                                                   Number of times device has been seen on other websites
deviceSeenAwayData                ``[['2018-10-01T14:55:12.318029Z', '2.2.2.2', 57]]``    List of data about the last 100 visits to the pages of other DataDriven's customers: (<start of visit in UTC>, <IP address used during visit>, <duration of visit in seconds>). If any info is missing, nothing is returned
deviceType                        ``mobile``                                              Type of the device (desktop/mobile/tablet/touch_device)
devicePrice [#f1]_                ``null``                                                Average price of the device
deviceEmailAssociation            ``['some@email.com']``                                  List of known email address associated with device, max 100 (based on past requests to this endpoint)
devicePhoneAssociation            ``['+55524236225']``                                    List of known phone numbers associated with device, max 100 (based on past requests to this endpoint)
locationGeocodingAccuracy         ``"EXACT_MATCH"``                                       Geocoding accuracy (EXACT_MATCH/HIGH_ACCURACY/MEDIUM_ACCURACY/UNKNOWN_ACCURACY)
locationAverageIncome [#f1]_      ``null``                                                Average monthly income in the location of the user
locationRentalPrice [#f1]_        ``null``                                                Average rent price for location (EUR per square meter)
locationRealEstatePrice [#f1]_    ``null``                                                Average price of real estate for location (EUR per square meter)
locationClassification [#f1]_     ``null``                                                Classification of the location, see :ref:`classification` for details
locationOtherCustomers            ``1``                                                   Number of other devices in 1km radius we have spotted
fraudTooFastSession               ``true``                                                Returns ``true`` if total session time is less than 10 seconds (true/false/null)
fraudIpChange                     ``false``                                               Returns ``true`` if the user's IP address changed during the session (true/false/null)
fraudBrowserTimeZone              ``true``                                                Returns ``true`` if the timezone of the borrower's device is not from the same country as the webpage's location (true/false/null)
fraudVPN  [#f1]_                  ``null``                                                Returns ``true`` if the user uses a VPN or proxy (true/false/null)
fraudUncommonLanguage             ``true``                                                Returns ``true`` if borrowers language is not common for the webpage (webpage language is configured by the client) (true/false/null)
fraudDistance                     ``4.5``                                                 Distance of geocoded input address from the location of the IP address (in km)
fraudCountryMismatch              ``true``                                                Returns ``true`` if IP address and geocoded address country do not match
behaviorTermsVisited              ``true``                                                Returns ``true`` if applicant has opened terms and conditions (depends on if Actions are configured properly, see :ref:`tracking-actions` ) (true/false)
behaviorSessionDuration           ``90``                                                  Total time applicant spent on the site in seconds during session (start time is the time of first page visit, end time is the time for the first request to this endpoint)
behaviorTypingSpeed               ``45``                                                  Applicant’s typing speed (key strokes per minute) during session
behaviorCopyPaste                 ``2``                                                   Number of fields where paste was used during the session
behaviorFieldChanges              ``4``                                                   Number of fields edited after initial input during session
emailDataCompromised [#f1]_       ``null``                                                Whether the input email has been compromised in any data breaches (true/false/null)
emailDataEmailExists              ``true``                                                Whether mail server confirms email is valid (true/false/null)
phoneNumberIsActive [#f3]_        ``true``                                                Phone number is listed as active (true/false)
phoneNumberCarrierName            ``Telekom AS``                                          The name of the company that carries this phone number
phoneNumberLineType               ``landline``                                            The phone number type (landline/mobile/voip)
phoneNumberCallerName             ``Maggie Smith``                                        Name of the owner of the phone number
phoneNumberCallerType             ``CONSUMER``                                            String indicating whether this caller is a business or consumer (business/consumer); only works for US phone numbers.
================================= ======================================================= ========================================================================================================


.. _metadata:

Metadata
++++++++

Metadata looks like this::

    "_meta": {
        "field_behaviours": [
            {
                "field_name": "name",
                "field_id": "name_field",
                "page_id": "login-page",
                "page_title": "Login",
                "user_id": "123",
                "focus_started_at": "2019-09-16T08:21:24.917085Z",
                "focus_ended_at": "2019-09-16T08:21:24.917106Z",
                "key_count": 0,
                "del_count": 32,
                "backspace_count": 1,
                "dwell_time_avg": null,
                "dwell_time_sd": null,
                "flight_time_count": 3,
                "flight_time_avg": null,
                "flight_time_sd": null,
                "digraph_count": 7,
                "digraph_avg": null,
                "digraph_sd": null,
                "trigraph_count": 8,
                "trigraph_avg": null,
                "trigraph_sd": null,
                "pause_count": 4,
                "pause_avg": null,
                "pause_sd": null,
                "paste_count": 6,
                "was_changed": true
            }
        ],
        "actions": [
            {
                "action_id": "clicked_on_terms",
                "started_at": "2019-09-16T08:20:14.122698Z",
                "page_id": "login-page",
                "page_title": "Login",
                "user_id": "123"
            }
        ],
        "fingerprints": [
            {
                "hash": "vOAWThVFpXYbzjvOtwseqsrWrIyJRbxcyrkEZHNisJATgrJmHdijwqmSZEWEumKJd",
                "components": {
                    "audio": 153.5435345345,
                    "fonts": " Arial,Courier,Courier New,Helvetica,MS Gothic,MS PGothic,Times,Times New Roman",
                    "webgl": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA",
                    "canvas": "canvas winding:yes,canvas fp:data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB9AAAADICAYAAACwGno",
                    "adBlock": false,
                    "plugins": "Chrome PDF Plugin,Portable Document Format,application/x-google-chrome-pdf,pdf,Chrome PDF Viewer",
                    "cpuClass": "not available",
                    "language": "en-GB",
                    "platform": "Linux x86_64",
                    "timezone": "Europe/London",
                    "hasLiedOs": true,
                    "indexedDb": true,
                    "userAgent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.35 Safari/53",
                    "colorDepth": 16,
                    "doNotTrack": false,
                    "fontsFlash": false,
                    "pixelRatio": "43:54",
                    "addBehavior": false,
                    "deviceMemory": 4,
                    "localStorage": false,
                    "openDatabase": false,
                    "touchSupport": "0,false,false",
                    "hasLiedBrowser": true,
                    "sessionStorage": false,
                    "timezoneOffset": "+240",
                    "enumerateDevices": true,
                    "hasLiedLanguages": false,
                    "screenResolution": false,
                    "hasLiedResolution": true,
                    "hardwareConcurrency": 16,
                    "webglVendorAndRenderer": "X.Org~Radeon RX 550 Series (POLARIS12 / DRM 3.23.0 / 4.15.0-43-generic, LLVM 6.0.0)",
                    "availableScreenResolution": [1053, 966]
                }
            }
        ],
        "page_visits": [
            {
                "arrived_at": "2019-09-16T08:22:00.950406Z",
                "left_at": "2019-09-16T08:22:00.950447Z",
                "page_id": "login-page",
                "page_title": "Login",
                "user_id": "123"
            },
            {
                "arrived_at": "2019-09-16T08:22:00.936041Z",
                "left_at": "2019-09-16T08:22:00.936063Z",
                "page_id": "login-page",
                "page_title": "Login",
                "user_id": "123"
            }
        ],
        "ip_addresses": ["1.2.2.45"],
        "end_time": "2019-09-16T08:23:28.035390Z"
    }


Metadata field definitions:


=================================  ============================================================================================================
Field name                         Description
=================================  ============================================================================================================
field_behaviours                   Data about how a user has been behaving in a single form field
actions                            Action data that we have received; must be configured to record any data - see :ref:`tracking-actions`
fingerprints                       Fingerprint data we get about the user; see fingerprintjs2 for details
page_visits                        Page visits gets recorded every time the user loads a page during a session
ip_addresses                       List of IP addresses used during the session
end_time                           Session end time (i.e. the time when the first request to /api/generic-data comes in)
=================================  ============================================================================================================

.. _classification:

Classification classes
++++++++++++++++++++++

\-


403 Forbidden
+++++++++++++

Returned if user has not authenticated themselves via the request headers or the query parameters
(see :doc:`authentication` for details).
Data will have the following message: ``Authentication credentials were not provided.``


.. rubric:: Footnotes

.. [#f1] These fields are not implemented yet and always return ``null``
.. [#f2] ``fraud`` fields return ``false`` if some data is missing (IP address, address, city, country, page visits)
.. [#f3] Always returns ``true`` for now
