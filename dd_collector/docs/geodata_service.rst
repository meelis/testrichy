Geodata service
===============

The geodata service offers an endpoint to retrieve useful information about an address and what it is in proximity to.

Setup
*****

Make sure you have a valid ``accountId`` for the service.


Retrieving data
***************

The address service results are available at

.. parsed-literal::

    |APIUrl|/api/address_surroundings?address={address}


.. role:: red

* ``address`` :red:`*` – string value (URL encoded) of the address You need information for.
* ``r1``, ``r2``, ``r3`` `[optional]` – custom integer radiuses (in meters) for which the data will be returned.
  Pass in either all three or none of them. **Min value** = 1, **max value** = 10 000.


:red:`*` - required

Example query
------------------

Example cURL query:

.. parsed-literal::

    url -X GET \
    '\ |APIUrl|\ /api/address_surroundings?
    address=KUMPULANTIE%203,%20FI-00520%20HELSINKI' \
    -H 'Authorization: Api-Token: 322a7635-aec4-4307-8c54-73cb7c043124'


Responses
---------

200 Success
+++++++++++

Returned if the user has been successfully authenticated and the geocoding service returns a response.

The data contains aggregated counts of each type of object that are within 10km range from the
geographic location of the input address. All objects are separated into three categories:

1. objects that are within 500 m of the object
2. objects that are within 500 m to 2 km of the object
3. objects that are within 2 km to 10 km of the object

**Note**: these radiuses will be overriden by custom ones if any were passed in with URL parameters.


Example response::

    {
        "address": "Kumpulantie 3, FI-00520 Helsinki, Finland, Finland",
        "accuracy_level": "HIGH_ACCURACY"
        "data": {
            "10000m": {
                "artwork": 345,
                "taxi": 85,
                ...
            },
            "2000m": {
                "cinema": 3,
                "tower": 3,
                ...
            },
            "500m": {
                "beverages": 1,
                "courthouse": 1,
                ...
            }
        }
    }

The data for each radius will be alphabetically sorted.

403 Forbidden
+++++++++++++

Returned if user has not authenticated themselves via the request headers or the query parameters (see :doc:`authentication` for details).
Data will have the following message: ``Authentication credentials were not provided.``


400 Bad Request
+++++++++++++++

Returned if input data is invalid:

* ``Please include address in query parameters`` – ``address`` was not found in the query parameters
* ``Please either provide all three radiuses [`r1`, `r2`, `r3`] or no radiuses in the parameters`` –
  only one or two custom radiuses were passed in (the service expects either all three or none)


502 Bad Gateway
++++++++++++++++++

Returned if:

* the geocoding service returns an empty response; if this happens, it is recommended that you try the request again.
* the geocoding service raises an error.

Details are in the response message.
