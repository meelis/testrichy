Data Collection services documentation
======================================


This service combines two pieces of functionality:

1. :doc:`Tracking service <tracking_service>`
2. :doc:`Geodata service <geodata_service>`


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   authentication
   Tracking service <tracking_service>
   Geodata service <geodata_service>
