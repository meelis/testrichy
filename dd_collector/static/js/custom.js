window.jQuery;
$(window).on("resize", function() {
    if ($(window).width() > 768) $("#sidebar-collapse").collapse("show");
});
$(window).on("resize", function() {
    if ($(window).width() <= 767) $("#sidebar-collapse").collapse("hide");
});

let element = document.querySelector(".clickable");
let sidebarCollapse = document.querySelector(".sidebar-collapse");
element.onclick = e => {
    if (e.target.getAttribute("aria-expanded") === "true")
        sidebarCollapse.style.display = "none";
    else sidebarCollapse.style.display = "block";
    e.target.setAttribute(
        "aria-expanded",
        e.target.getAttribute("aria-expanded") === "false" ? "true" : "false"
    );
};
// 140vw
