$(function () {

    var sidebar = $('#doc-sidebar');
    var top = sidebar.offset().top - parseFloat(sidebar.css('margin-top'));
    $(window).scroll(function (event) {
    var y = $(this).scrollTop();
    
    if (y >= top && $(window).width() >= 768) {
        sidebar.addClass('sticky');
    } else {
        sidebar.removeClass('sticky');
    }
    });
});