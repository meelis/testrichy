# Generated by Django 2.0.13 on 2019-12-28 11:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scrapper', '0004_domypl'),
    ]

    operations = [
        migrations.RenameField(
            model_name='domypl',
            old_name='longitude',
            new_name='longtitude',
        ),
    ]
