def remove_currency_symbols(price):
    if price is not None:
        right_price = price.replace(
            'PLN/m²', "").replace(",", ".").replace(" ", "")
        return float(right_price)


def remove_street(address):
    right_address = address.replace('Ul. ', "").replace("ul. ", "").strip()
    if "Wierzbno" == right_address.split(', ')[-2]:
        right_address = right_address.split(
            ', ')[0] + ', ' + right_address.split(', ')[-1]
    return right_address


def get_page(page):
    next_page = page.replace("/mieszkania--lodzkie-pl", "")
    return next_page
