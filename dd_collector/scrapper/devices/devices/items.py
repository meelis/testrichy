import scrapy


class ScraperItem(scrapy.Item):
    transaction_type = scrapy.Field()
    offer_link = scrapy.Field()
    address = scrapy.Field()
    price_per_square_meter = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
