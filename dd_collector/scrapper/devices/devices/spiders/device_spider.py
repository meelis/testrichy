import scrapy
import time
from scrapper.models import MobileModel, MobileAttr, MobileItem
from djmoney.money import Money
import datetime
import re


class DeviceSpider(scrapy.Spider):
    name = 'device'
    meta = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}
    start_urls = [
        'https://www.gsmarena.com/makers.php3',
    ]
    custom_settings = {
        'DOWNLOAD_DELAY': 100,
        'COOKIES_ENABLED': False,

    }

    def start_requests(self):
        url = 'https://www.gsmarena.com/makers.php3'
        yield scrapy.Request(url, meta=self.meta, callback=self.parse)

    def parse(self, response):
        for href in response.css('.st-text a::attr(href)'):
            yield response.follow(href, meta=self.meta,
                                  callback=self.parse_phones)

    def parse_phones(self, response):
        for href in response.css('#review-body a::attr(href)'):
            yield response.follow(href, meta=self.meta,
                                  callback=self.parse_device_info)

        next_page = response.css('.pages-next::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield response.follow(next_page, meta=self.meta,
                                  callback=self.parse_phones)

    def parse_device_info(self, response):
        price = response.css(
            'td[data-spec=price] a::text').get() or response.css('td[data-spec=price]::text').get()
        name = response.css('.specs-phone-name-title::text').get()
        models = response.css('td[data-spec=models]::text').get()
        if models:
            models = models.split(', ')
        else:
            models = ['{}'.format(name)]
        launch = response.css('td[data-spec=year]::text').get()
        if launch:
            year = re.search(r'\d{4}', launch)
            if year:
                year = int(year.group())
                month = re.search('Q\d{1}', launch)
                if month:
                    month = {
                        'Q4': 11,
                        'Q3': 8,
                        'Q2': 5,
                        'Q1': 1
                    }[month.group()]
                    obj = MobileModel.objects.create(
                        name=name, launch=datetime.date(year, month, 1))
                else:
                    arr_monthes = ['January', 'February', 'March', 'April',
                                   'May', 'June',
                                   'July', 'August', 'September', 'October',
                                   'November', 'December']
                    for i in arr_monthes:
                        if i in launch:
                            month = i
                            break
                    if month:
                        launch = launch.split(', ')
                        obj = Object.objects.create(name=name,
                                                    launch=datetime.date(
                                                        year,
                                                        time.strptime(launch[1][:3], '%b').tm_mon, 1))
                    else:
                        obj = Object.objects.create(
                            name=name, launch=datetime.date(year, 1, 1))
        else:
            obj = MobileModel.objects.create(name=name)
        for m in models:
            item = MobileItem.objects.create(obj=obj, name=m)
            if price:
                if 'eur' in price.lower():
                    MobileAttr.objects.create(item=item, price=Money(
                        float(price.split(' ')[1]), 'EUR'))
                elif 'usd' in price.lower():
                    MobileAttr.objects.create(item=item, price=Money(
                        float(price.split(' ')[1]), 'USD'))
                elif 'about' not in price.lower():
                    price = price.split(' / ')
                    price = list(map(lambda x: x.replace('\u2009', ''), price))
                    for p in price:
                        price_currency = p[0]
                        price_amount = float(p.split('{}'.format(p[0]))[1])
                        bank_symbols_currency = ''
                        if price_currency == '$':
                            bank_symbols_currency = 'USD'
                        elif price_currency == '€':
                            bank_symbols_currency = 'EUR'
                        elif price_currency == '£':
                            bank_symbols_currency = 'GBP'
                        elif price_currency == '₹':
                            bank_symbols_currency = 'INR'
                        if bank_symbols_currency:
                            MobileAttr.objects.create(item=item, price=Money(
                                price_amount, bank_symbols_currency))
