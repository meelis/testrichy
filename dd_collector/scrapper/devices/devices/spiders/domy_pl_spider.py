import scrapy
from scrapy import Request
from geopy.geocoders import Nominatim
from scrapper.models import DomyPl
from scrapper.devices.devices.items import ScraperItem
from scrapper.devices.devices.utils import (
    remove_currency_symbols,
    remove_street,
    get_page
)


class DomyPlSpider(scrapy.Spider):
    name = "domy_pl"
    allowed_domains = ['domy.pl']
    start_urls = [
        "https://domy.pl/mieszkania-sprzedaz--pl",
        # "https://domy.pl/mieszkania-wynajem--pl"
        # "https://domy.pl/domy-sprzedaz--pl",
        # "https://domy.pl/domy-wynajem--pl"
    ]

    def parse(self, response):
        geolocator = Nominatim(user_agent="domy_pl")
        items = ScraperItem()

        for department in response.xpath('//section[@class="tab"]/article'):
            transaction_type = department.xpath(
                './/span[@class="description"]/text()').get()
            offer_link = department.xpath(
                './/div[@class="headerTextBox"]/a/@href').get()
            address = department.xpath(
                './/div[@class="headerTextBox"]/a/text()').get()
            price_per_square_meter = department.xpath(
                './/span[@class="pricem2"]/text()').get()

            items["transaction_type"] = 1 if "sprzedaż" in transaction_type else 2
            items["offer_link"] = offer_link
            items["address"] = address.strip()
            items["price_per_square_meter"] = remove_currency_symbols(
                price_per_square_meter)

            location = geolocator.geocode(remove_street(address), timeout=60)
            items["latitude"] = float(location.latitude)
            items["longitude"] = float(location.longitude)
            DomyPl.objects.create(transaction_type=items['transaction_type'],
                                offer_link=items['offer_link'],
                                address=items['address'],
                                price_per_square_meter=items['price_per_square_meter'],
                                latitude=items['latitude'],
                                longitude=items['longitude'])
            yield items

        next_page = response.xpath(
            '//div[@class="paginator"]/a[@class="current"]/following-sibling::a/@href').get()
        if next_page is not None:
            yield scrapy.Request(response.urljoin(get_page(next_page)),
                                 self.parse)
