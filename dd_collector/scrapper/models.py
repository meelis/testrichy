from django.db import models
from djmoney.models.fields import MoneyField


class DomyPl(models.Model):
    transaction_type = models.IntegerField()
    offer_link = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    price_per_square_meter = models.FloatField()
    latitude = models.FloatField()
    longitude = models.FloatField()

    class Meta:
        verbose_name = 'Domy'

    def __str__(self):
        return self.offer_link


class MobileModel(models.Model):
    name = models.CharField(max_length=150)
    launch = models.DateField(
        auto_now=False, auto_now_add=False, blank=True, null=True)

    class Meta:
        verbose_name = 'MobileModel'
        verbose_name_plural = 'MobileModels'

    def __str__(self):
        return self.name


class MobileItem(models.Model):
    obj = models.ForeignKey(MobileModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'MobileItem'
        verbose_name_plural = 'MobileItems'

    def __str__(self):
        return self.name


class MobileAttr(models.Model):
    item = models.ForeignKey(MobileItem, on_delete=models.CASCADE)
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='USD')

    def __str__(self):
        return 'Attr for ' + self.item.name

    class Meta:
        verbose_name = 'MobileAttr'
        verbose_name_plural = 'MobileAttrs'
