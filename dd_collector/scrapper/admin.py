from django.contrib import admin
from scrapper.models import MobileModel, MobileItem, MobileAttr, DomyPl

admin.site.register(MobileModel)
admin.site.register(MobileItem)
admin.site.register(MobileAttr)
admin.site.register(DomyPl)
