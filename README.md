# README for dd_collector

## Usage

See `/docs` for details on installation and usage.


## Data after deployment

In order to have initial data for countries after initial deployment, run
```
docker-compose run --rm django ./manage.py refresh_countries
```

This fetches download information from GeoFabrik and saves it to the database.

This task is set to run periodically once a month, so no further action
is needed after initial deployment.


## Base URL change


If the base URL for the API changes, change the `DJANGO_SITE_URL` environment variable to the new URL
(for example, `DJANGO_SITE_URL=https://www.new-domain.example.com`), and then make sure to run `make setup`.

Otherwise the front-end app will not receive the info of where it needs to POST tracking data.


[![Build status](https://gitlab.com/thorgate/dd_collector/badges/master/pipeline.svg)](git@gitlab.com:thorgate/data-driven-collector.git/commits/master)
[![Coverage report](https://gitlab.com/thorgate/dd_collector/badges/master/coverage.svg)](git@gitlab.com:thorgate/data-driven-collector.git/commits/master)

Browser support is defined in the `dd_collector/browserslist` file that is used for autoprefixing CSS.


## Setting up development

### Installing Docker and Docker Compose

Refer to original [Docker documentation](https://docs.docker.com/engine/installation/) for installing Docker.

After installing Docker you need to install [Docker Compose](https://docs.docker.com/compose/install/) to run
 multi-container Docker applications (such as ours). The `curl` method is preferred for installation.

To run Docker commands without `sudo`, you also need to
[create a Docker group and add your user to it](https://docs.docker.com/engine/installation/linux/ubuntulinux/#/create-a-docker-group).

### Installing pipenv

To ensure correct project dependencies are resolved `pipenv` is required to be installed locally.
Easiest way is to create virtualenv for this.
Keep this active when doing `make setup` or any time adding dependencies to project.

Check commands info for `pipenv` helpers.

### Setting up dd_collector

The easy way is to use `make` to set up everything automatically:

    make setup

This command:

- copies PyCharm project directory
- creates local settings file from local.py.example
- builds Docker images
- sets up database and runs Django migrations
- runs `docker-compose up`

Refer to `Makefile` to see what actually happens. You can then use the same commands to set everything up manually.


To load required data:

1. `manage.py refresh_countries`
1. Either mark a country as active in admin, or do it manually.


## Running development server

Both docker and docker-compose are used to run this project, so the run command is quite straightforward.

    docker-compose up

This builds, (re)creates and starts containers for Django, Node, PostgreSQL and Redis. Refer to `docker-compose.yml` for
more insight.

Logs from all running containers are shown in the terminal. To run in "detached mode", pass the `-d` flag to
docker-compose. To see running containers, use `docker-compose ps`. To see logs from these containers, run
`docker-compose logs`.

To _stop_ all running containers, use

    docker-compose stop

This stops running containers without removing them. The same containers can be started again with
`docker-compose start`. To stop a single container, pass the name as an extra argument, e.g.
`docker-compose stop django`.

To _stop and remove_ containers, run

    docker-compose down

This stops all running containers and removes containers, networks, volumes and images created by `up`.

### Using a different configuration file

By default docker-compose uses the `docker-compose.yml` file in the current directory. To use other configuration files,
e.g. production configuration, specify the file to use with the `-f` flag.

    docker-compose -f docker-compose.production.yml up

Note that the production configuration lacks PostgreSQL, since it runs on a separate container on our servers.

## Running Django commands in Docker

    docker-compose run django python manage.py <command>

### Command shortcuts in the Makefile

|Action                                |Makefile shortcut                      |Actual command                                                              |
|:-------------------------------------|:--------------------------------------|:---------------------------------------------------------------------------|
|Installing Python dependencies        |`make py-install-deps cmd=<dependency>`|`pipenv --python=$(PYTHON) install $(cmd)`                                  |
|Generate Pipfile.lock                 |`make Pipfile.lock`                    |`pipenv --python=$(PYTHON) lock`                                            |
|Check Python package security warnings|`make pipenv-check`                    |`pipenv --python=$(PYTHON) check`                                           |
|make migrations                       |`make makemigrations cmd=<command>`    |`docker-compose run --rm django ./manage.py makemigrations $(cmd)`          |
|migrating                             |`make migrate cmd=<command>`           |`docker-compose run --rm django ./manage.py migrate $(cmd)`                 |
|manage.py commands                    |`make docker-manage cmd=<command>`     |`docker-compose run --rm django ./manage.py $(cmd)`                         |
|any command in Django container       |`make docker-django cmd=<command>`     |`docker-compose run --rm django $(cmd)`                                     |
|run tests                             |`make test`                            |`docker-compose run --rm django py.test`                                    |
|run linters                           |`make quality`                         |                                                                            |
|run StyleLint                         |`make stylelint`                       |`docker-compose run --rm node yarn stylelint`                            |
|run ESLint                            |`make eslint`                          |`docker-compose run --rm node yarn lint`                                 |
|run Prospector                        |`make prospector`                      |`docker-compose run --rm django prospector`                                 |
|run psql                              |`make psql`                            |`docker-compose exec postgres psql --user dd_collector --dbname dd_collector` |
|generate docs                         |`make docs`                            |`docker-compose run --rm django sphinx-build ./docs ./docs/_build`          |

## Running commands on the server

    docker-compose -f docker-compose.production.yml run --rm --name dd_collector_tmp django python manage.py <command>

## Notebooks

Notebook server is available on localhost port 8001.
For access to django database etc in a notebook see NotebookExample.


## Installing new pip or npm packages

Since `yarn` is inside the container, currently the easiest way to install new packages is to add them
to the `package.json` file and rebuild the container.

Python dependencies are a bit special. As we are using `pipenv` best option is to use `make py-install-deps cmd=<dependency>`.
After package is added also don't forget to `make Pipfile.lock` to update lock file. This ensure when we are building production images
we don't install conflicting packages and everything is resolved to matching version while developing.

When using `pipenv` via make file it will create the virtualenv under project directory. To use `pipenv` manually without `Makefile`,
prefix `pipenv` commands with `PIPENV_VENV_IN_PROJECT=1`.

## Rebuilding Docker images

To rebuild the images run `docker-compose build`. This builds images for all containers specified in the configuration
file.

To rebuild a single image, add the container name as extra argument, e.g. `docker-compose build node`.

## Swapping between branches

After changing to a different branch, run `docker-compose up --build`. This builds the images before starting
containers.

If you switch between multiple branches that you have already built once, but haven't actually changed any configuration
(e.g. installed new pip or npm packages), Docker finds the necessary steps from its cache and doesn't actually build
anything.

## Running tests

You can also use `--reuse-db` or `--nomigrations` flags to the actual command above to speed things up a bit. See also:
https://pytest-django.readthedocs.org/en/latest/index.html


## Generating documentation with Sphinx

To build **.rst** files into html, run `make docs`. View the documentation at `/docs/_build/index.html`.
Read more about contributing to docs from `/docs/contributing.rst`.

Documentation is generated from the reStructuredText
files in /docs/\*.rst. See the reference for reStructuredText Primer
[here](http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>).

**note**: PyCharm 2018.2 now has support for reST Preview, although it doesn't work well with Docker.

New files should be added to /docs with the **.rst** extension.
Then update the toctree in docs/index.rst.

Run `make docs` to generate html.

Use [autodoc](http://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html) to automatically add documentation 
from docstring.

The docs should be available at `/docs` (mounted with the help of django-docs package) after running `make docs`.


### Coverage

You can also calculate tests coverage with `coverage run -m py.test && coverage html`,

TODO: Expose this directory outside of docker
the results will be in `cover/` directory.


## Running linters

Linters check your code for common problems. Running them is a good idea before submitting pull requests, to ensure you
don't introduce problems to the codebase.

We use _ESLint_ (for JavaScript parts) and _Prospector_ (for Python) and StyleLint (for SCSS).
To use them, run those commands in the Django app dir:

    # Check Javascript sources with ESLint:
    make eslint
    # Check SCSS sources with StyleLint:
    make stylelint
    # Check Python sources with Prospector:
    make prospector
    # Run all of above:
    make quality


## Django translations

Project contains two commands for updating and compiling translations. Those two are `make makemessages` and `make compilemessages`.
Howewer if you are adding a new language or are creating translations for the first time after setting up project, you need to run
different command to create the initial locale files. The command is `add-locale`. After you have used this command once per each
new language you can safely use `makemessages` and `compilemessages`


## Load test results

### April 2019

When deployed alone to a digital-ocean s-4vcpu-8gb we can handle about ~100 requests/s, 
This is with CPU/RAM/Network to spare, and could most likely be increased
with more gunicorn workers.


Locust results, percentile response times (ms):

```
 Name                                                           # reqs    50%    66%    75%    80%    90%    95%    98%    99%   100%
--------------------------------------------------------------------------------------------------------------------------------------------
 GET /api/behaviour-data []                                        928     97    100    110    120    140    170    250    300   1000
 GET /api/behaviour-data [country,address,phone]                  1250    100    140    260   1100   1200   1200   1200   1300   1600
 GET /api/behaviour-data [country,address]                         614    100    140    330   1100   1200   1200   1200   1300   1500
 GET /api/behaviour-data [country,phone]                          2503     96    100    110    120    150    200    270    330    720
 GET /api/behaviour-data [country]                                1212     97    110    120    130    150    200    260    320   1200
 GET /api/behaviour-data [phone]                                  1913     96    100    110    120    150    170    240    300    960
 GET /b.gif [field_behaviour]                                   167769     77     83     90     99    130    160    230    300   4200
 POST /fp.jpeg [fingerprint]                                     33350     73     80     86     94    120    150    220    300   1300
 GET /pv.jpg [pagevisit]                                         33341     73     79     85     93    120    150    220    290   1400
 GET /z.gif [action]                                             33342     73     79     86     93    120    150    230    300   4200
--------------------------------------------------------------------------------------------------------------------------------------------
 Total                                                          276222     76     83     91     99    130    160    240    310   4200
```

Here `time.sleep(1)` was used in place of geocode api calls.


## Deploys

### Python 2 environment

We use Fabric for deploys, which doesn't support Python 3. Thus you need to create a Python 2 virtualenv.
It needn't be project specific and it's recommended you create one 'standard' Python 2 environment
which can be used for all projects. You will also need to install django and tg-hammer==0.6, our fabric deployment helper.


### Server setup

Your server needs to have [Docker Engine](https://docs.docker.com/engine/installation/)
as well as [Docker Compose](https://docs.docker.com/compose/) installed.

We also assume that you have Nginx and Postgres (version 10 by default) running in Docker containers and reachable via
'private' network. We also make a few assumptions regards directories that will be used as volumes for static assets,
etc. You can find these paths in `fabfile.py` and `docker-compose.production.yml`.


### Types of deploys

There are basically two types of deploys:

* initial deploy, where the project doesn't exist in the server yet.
* incremental deploy, where the project only needs to be updated.


### Incremental deploy

* Ensure that whatever you want deployed is committed and pushed.
* Just run `fab ENV deploy` where `ENV` is either `test` or `live`.
  You'll see the changes to be applied and can continue or abort.
  * You can specify revision (either id or branch name) by running `fab ENV deploy:id=REV`
    Future deploys will stick to the same branch/rev and you'll need to explicitly deploy master/default
    branch to get back to it.


### Initial deploy

* Figure out which server you're going to deploy to.
  We usually have one main test server and one main production server for new project.
* Check `fabfile.py` in Django project dir. It has two tasks (functions) - `test` and `live`.
  Ensure that the one you'll use has correct settings (mostly hostname).
* Check django settings (`settings/staging.py` and/or `settings/production.py`)
  and Nginx config (`deploy/nginx/*.conf`, `deploy/letsencrypt/*.conf`) - ensure that they have proper hostnames etc.
* Add the server's SSH key (`/root/.ssh/id_rsa.pub`) to the project repo as deployment key.
* Ensure you've committed and pushed all relevant changes.
* [Create the bucket for media files](http://docs.aws.amazon.com/AmazonS3/latest/UG/CreatingaBucket.html):
  * Bucket name: dd_collector-{ENV} where `ENV` is either `staging` or `production`.
  * Region: Closest to the users of the project.
  * Create a new user:
    * Go to [AWS IAM](https://console.aws.amazon.com/iam/home?#users).
    * Click "Create new users" and follow the prompts.
    * Leave "Generate an access key for each User" selected.
  * Get the credentials:
    * Go to the new user's Security Credentials tab.
    * Click "Manage access keys".
    * Download the credentials for the access key that was created.
    * and Save them somewhere because no one will ever be able to download them again.
    * Get the new user's ARN (Amazon Resource Name) by going to the user's Summary tab.
       It'll look like this: "arn:aws:iam::123456789012:user/someusername".
  * Go to the bucket properties in the [S3 management console](https://console.aws.amazon.com/s3/home).
  * Add a bucket policy that looks like this, but change "BUCKET-NAME" to the bucket name,
     and "USER-ARN" to your new user's ARN. This grants full access to the bucket and
     its contents to the specified user:

    {
        "Statement": [
            {
                "Action": "s3:*",
                "Effect": "Allow",
                "Resource": [
                    "arn:aws:s3:::BUCKET-NAME/*",
                    "arn:aws:s3:::BUCKET-NAME"
                ],
                "Principal": {
                    "AWS": [
                        "USER-ARN"
                    ]
                }
            }
        ]
    }

  * More information about working with S3 can be found [here](https://github.com/Fueled/django-init/wiki/Working-with-S3).

* Run `fab ENV setup_server` where `ENV` is either `test` or `live`.
  * If migrations failed due to access to postgres db, hardcode the password to dd_collector in the settings file.
  * If it worked, you're all done, congrats!
  * If something else broke, you might need to either nuke the code dir, database and database user on the server;
    or comment out parts of fabfile (after fixing the problem) to avoid trying to e.g. create database twice. Ouch.


### Updating packages in pipenv

* make sure you have pipenv installed
* Update packages in Pipenv file
* run `pipenv lock` if it successfully generates lock file, then you are set
* if previous command fails (due to package version clash), then do as it suggests - install the packages using the commands given and see what version is installed.


### Using pipenv locally for pycharm

* run `pipenv install` locally. Given, that you have pipenv installed.
* When you ran previous command, it told you where it created the virtual environment something like /home/you/.virtualenvs/projectname-somehash
* if you missed it you can see it by running `pipenv run which python`
* Open your project in pycharm and under settings search of project interpreter or just interpreter. Pycharm is smart enough and should already have picked up your venv location but just in case you can make sure it matches the path you saw when you ran the install command
